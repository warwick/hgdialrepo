/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* Usage:
Main Usage Scenario 1. Necessary if you want dials to interact with each other.
1.
hgDial1 = (HGDial) rootView.findViewById(R.id.coolDialBg);//Construct with LayoutInflater

2.
public class CoolDialFragment extends Fragment implements HGDial.OnTouchListener {
    @Override
    public boolean onTouch(final View v, final MotionEvent event) {
        if(v.getId() == R.id.coolDialBg) {
            hgDial1.registerCallback(new HGDial.IHGDial() {
                @Override
                public void onDown(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onPointerDown(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onMove(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onPointerUp(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onUp(HGDial.HGDialInfo hgDialInfo) {}
            });
            hgDial1.sendTouchEvent(v, event);//Necessary line
        }
        else if(v.getId() == R.id.coolDialFg) {
            hgDial2.registerCallback(new HGDial.IHGDial() {
                @Override
                public void onDown(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onPointerDown(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onMove(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onPointerUp(HGDial.HGDialInfo hgDialInfo) {}

                @Override
                public void onUp(HGDial.HGDialInfo hgDialInfo) {}
            });
            hgDial2.sendTouchEvent(v, event);//Necessary line
        }//End if(v.getId() == R.id.coolDialBg)
        return true;
    }//End public boolean onTouch(final View v, final MotionEvent event)
}

Usage Scenario 2. Quick implementation for single Dial
1. implement HGDial.IHGDial in your class,
2. hgDial1 = (HGDial) rootView.findViewById(R.id.coolDialBg);//Construct with LayoutInflater,
3. Then hgDial1.registerCallback(this);

Shorthand usage for single dial. (handy and quick if you use code completion) no need for Class implementation
hgDial.registerCallback(new HGDial.IHGDial() {
	@Override
	public void onDown(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onPointerDown(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onMove(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onPointerUp(HGDial.HGDialInfo hgDialInfo) {}

	@Override
	public void onUp(HGDial.HGDialInfo hgDialInfo) {}
});
*/
package com.WarwickWestonWright.HGDial;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import java.lang.ref.WeakReference;

public class HGDial extends View implements Runnable {

	/* Top of block field declarations */
	//Developer note: try to get the acting object to work by making automatic var copies of currentGestureAngleBaseOne, fullObjectAngleBaseOne and precisionRotation
	private final HGDialHandler hgDialHandler = new HGDialHandler(this);
	private boolean viewIsSetup = false;
    private OnTouchListener onTouchListener;
    private int contentWidth;
    private int contentHeight;
	private int paddingLeft;
	private int paddingTop;
	private int paddingRight;
	private int paddingBottom;

	private int leftMargin;
	private int topMargin;
	private int rightMargin;
	private int bottomMargin;

    private double rapidDial;
    private IHGDial ihgDial;
    private final HGDialInfo hgDialInfo = new HGDialInfo();
    private final AngleWrapper angleWrapper = new AngleWrapper();
    private float firstTouchX;
    private float firstTouchY;
    private float secondTouchX;
    private float secondTouchY;
    private int touchPointerCount;
    private double storedTextureAngle;
    private double currentTextureAngle;
    private double fullTextureAngle;
	private double precisionRotation;
	private double storedPrecisionRotation;
    private boolean cumulativeRotate;
    private boolean useSingleFinger;
	private boolean tooManyTouches;
	private boolean touchesSwapped;
    private double onDownCumulativeTextureAngle;
    private double onUpGestureAngle;
    private double angleSnap;
	private double angleSnapNextOld;
    private double angleSnapNext;
    private double angleSnapProximity;
    private boolean rotateSnapped;
    private double maximumRotation;
    private double minimumRotation;
    private boolean useMinMaxRotation;
    private int minMaxRotationOutOfBounds;
    private float touchOffsetX;
    private float touchOffsetY;
    private float touchXLocal;
    private float touchYLocal;
    private long quickTapTime;
    private boolean suppressInvalidate;
    private double variableDialInner;
    private double variableDialOuter;
    private int imageRadius;
    private boolean useVariableDial;

	//Fling Spin Variables
	private float spinStartSpeed;
	private float spinEndSpeed;
	private long spinDuration;
	private float spinStartSpeedState;
	private float spinEndSpeedState;
	private long spinDurationState;
	private float slowFactor;
	private boolean slowDown;
	private float flingAngleTolerance;
	private long spinEndTime;
	private double spinStartAngle;//Used to calculate start speed
	private double fullTextureDownAngle;//Used to trigger spin based upon angle moved rather than flingDistanceThreshold. Used when flingAngleTolerance is non zero
	private long gestureDownTime;//Also used for quick tap
	private float flingDownTouchX;
	private float flingDownTouchY;
	private int flingDistanceThreshold;
	private long flingTimeThreshold;
	private volatile boolean spinTriggered;
	private boolean spinTriggeredProgrammatically;
	private int lastTextureDirection;
	private Thread spinAnimationThread;

	/* useVariableDialCurve set to true to make the variable dial accelerate/decelerate on a curve see also variable below positiveCurve */
	private boolean useVariableDialCurve;
	/* positiveCurve set to true to cause variable dial to decelerate towards the outer dial false to decelerate towards inner dial see variable above */
    private boolean positiveCurve;

	private int imageRadiusX2;//This variable only exists for optimisation purposes for use with the getVariablePrecisionCurve method
    private Drawable drawable;
    /* Bottom of block field declarations */

    /* Top of block field declarations */
    public interface IHGDial {

        void onDown(final HGDialInfo hgDialInfo);
        void onPointerDown(final HGDialInfo hgDialInfo);
        void onMove(final HGDialInfo hgDialInfo);
        void onPointerUp(final HGDialInfo hgDialInfo);
        void onUp(final HGDialInfo hgDialInfo);

    }

	/* Top of block constructors */
    public HGDial(Context context) {
        super(context);

        setOnTouchListener(getOnTouchListerField());
        init(null, 0);
        setFields();

    }//End public HGDial(Context context)


    public HGDial(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnTouchListener(getOnTouchListerField());
        init(attrs, 0);
        setFields();

    }//End public HGDial(Context context, AttributeSet attrs)
	/* Bottom of block constructors */


    private void setFields() {

		this.viewIsSetup = false;
        this.contentWidth = 0;
        this.contentHeight = 0;
		this.paddingLeft = 0;
		this.paddingTop = 0;
		this.paddingRight = 0;
		this.paddingBottom = 0;
		this.leftMargin = 0;
		this.topMargin = 0;
		this.rightMargin = 0;
		this.bottomMargin = 0;
        this.rapidDial = 0d;
		this.ihgDial = null;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.storedTextureAngle = 0d;
        this.currentTextureAngle = 0d;
        this.fullTextureAngle = 0d;
        this.fullTextureDownAngle = 0d;
        this.touchPointerCount = 0;
        this.cumulativeRotate = true;
        this.useSingleFinger = true;
		this.tooManyTouches = false;
		this.touchesSwapped = false;
        this.onDownCumulativeTextureAngle = 0d;
        this.onUpGestureAngle = 0d;
        this.precisionRotation = 1d;
        this.angleSnap = 0d;
		this.angleSnapNextOld = 0d;
        this.angleSnapProximity = 0d;
        this.rotateSnapped = false;
        this.maximumRotation = 0d;
        this.minimumRotation = 0d;
        this.useMinMaxRotation = false;
        this.minMaxRotationOutOfBounds = 0;
        this.touchOffsetX = 0;
        this.touchOffsetY = 0;
        this.touchXLocal = 0f;
        this.touchYLocal = 0f;
        this.quickTapTime = 75L;
        this.suppressInvalidate = false;
        this.variableDialInner = 0d;
        this.variableDialOuter = 0d;
        this.useVariableDial = false;
		this.useVariableDialCurve = false;
		this.spinStartSpeed = 0;
		this.spinEndSpeed = 0;
		this.spinDuration = 0L;
		this.spinStartSpeedState = spinStartSpeed;
		this.spinEndSpeedState = spinEndSpeed;
		this.spinDurationState = spinDuration;
		this.slowFactor = 0f;
		this.slowDown = true;
		this.flingAngleTolerance = 0f;
		this.spinEndTime = 0L;
		this.spinStartAngle = 0d;
		this.gestureDownTime = 0L;
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0L;
		this.spinTriggered = false;
		this.lastTextureDirection = 0;
		this.positiveCurve = false;
		this.imageRadiusX2 = imageRadius * 2;
        this.angleSnapNext = 0d;

    }//End private void setFields()

    public void registerCallback(IHGDial ihgDial) {this.ihgDial = ihgDial;}

	/* Top of block Boilerplate overrides for extended view */
    private void init(AttributeSet attrs, int defStyle) {

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGDial, defStyle, 0);

        if(a.hasValue(R.styleable.HGDial_foregroundDrawable)) {
            drawable = a.getDrawable(R.styleable.HGDial_foregroundDrawable);
            drawable.setCallback(this);
        }

        a.recycle();

    }//End private void init(AttributeSet attrs, int defStyle)

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

		if(viewIsSetup == true) {

			if(rapidDial != 0) {
				canvas.rotate((float) rapidDial * 360, angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y);
				rapidDial = 0;
			}
			else if(angleSnap == 0) {
				canvas.rotate((float) (angleWrapper.getTextureAngle() * 360), angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y);
			}
			else if(angleSnap != 0) {
				canvas.rotate((float) (angleSnapNext * 360), angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y);
			}//End if(rapidDial != 0)

			drawable.draw(canvas);

		}
		else if(viewIsSetup == false) {

			//paddingLeft = getPaddingLeft();
			//paddingTop = getPaddingTop();
			//paddingRight = getPaddingRight();
			//paddingBottom = getPaddingBottom();
			FrameLayout parentLayout = (FrameLayout) getParent();
			RelativeLayout.LayoutParams parentParams = (RelativeLayout.LayoutParams) parentLayout.getLayoutParams();
			leftMargin = parentParams.leftMargin;
			topMargin = parentParams.topMargin;
			rightMargin = parentParams.rightMargin;
			bottomMargin = parentParams.bottomMargin;

			if(getWidth() < getHeight()) {

				contentWidth = getWidth() - leftMargin - rightMargin;
				contentHeight = contentWidth;
				drawable.setBounds(leftMargin, (getHeight() - contentHeight) / 2, getWidth() - rightMargin, ((getHeight() - contentHeight) / 2) + contentHeight);
				imageRadius = (int) ((float) contentWidth / 2f);

			}
			else if(getWidth() >= getHeight()) {

				contentHeight = getHeight() - topMargin - bottomMargin;
				contentWidth = contentHeight;
				drawable.setBounds((getWidth() - contentWidth) / 2, topMargin, ((getWidth() - contentWidth) / 2) + contentWidth ,topMargin + contentHeight);
				imageRadius = (int) ((float) contentHeight / 2f);

			}//End if(getWidth() < getHeight())

			angleWrapper.viewCenterPoint.x = (contentWidth / 2);
			angleWrapper.viewCenterPoint.y = (contentHeight / 2);
			imageRadiusX2 = imageRadius * 2;
			angleWrapper.setViewCenterPoint(drawable.getBounds().left + (drawable.getBounds().width() / 2),   drawable.getBounds().top + (drawable.getBounds().height() / 2)  );
			viewIsSetup = true;

			if(drawable != null) {

				if(angleSnap == 0) {

					canvas.rotate((float) (angleWrapper.getTextureAngle() % 1 * 360), angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y);

				}
				else if(angleSnap != 0) {

					canvas.rotate((float) (angleSnapNext * 360), angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y);

				}//End if(angleSnap == 0)

				drawable.draw(canvas);

			}//End if(drawable != null)

		}//End if(viewIsSetup == true)

    }//End protected void onDraw(final Canvas canvas)
	/* Bottom of block Boilerplate overrides for extended view */


    /* Top of block touch methods */
    private void setDownTouch(final MotionEvent event) {

        try {
            try {

				if(useSingleFinger == true) {
					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgDialInfo.setFirstTouchX(firstTouchX);
					hgDialInfo.setFirstTouchY(firstTouchY);
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetY;
                }
                else /* if(useSingleFinger == false) */ {
					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgDialInfo.setFirstTouchX(firstTouchX);
					hgDialInfo.setFirstTouchY(firstTouchY);
					hgDialInfo.setSecondTouchX(secondTouchX);
					hgDialInfo.setSecondTouchY(secondTouchY);
                }//End if(useSingleFinger == true)

            }
            catch(IndexOutOfBoundsException e) {return;}
        }
        catch(IllegalArgumentException e) {return;}

    }//End private void setDownTouch(final MotionEvent event)

    private void setMoveTouch(final MotionEvent event) {

        try {
            try {
				if(useSingleFinger == true) {
                    firstTouchX = event.getX(event.findPointerIndex(0)) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(0)) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    secondTouchX = touchOffsetX;
                    secondTouchY = touchOffsetY;
                }
                else /* if(useSingleFinger == false) */ {
                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                    secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                    hgDialInfo.setFirstTouchX(firstTouchX);
                    hgDialInfo.setFirstTouchY(firstTouchY);
                    hgDialInfo.setSecondTouchX(secondTouchX);
                    hgDialInfo.setSecondTouchY(secondTouchY);
                }//End if(useSingleFinger == true)

            }
            catch(IndexOutOfBoundsException e) {return;}
        }
        catch(IllegalArgumentException e) {return;}

    }//End private void setMoveTouch(final MotionEvent event)


    private void setUpTouch(final MotionEvent event) {

        try {
            try {
				if(useSingleFinger == true) {
					firstTouchX = event.getX(event.findPointerIndex(0)) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(0)) + touchOffsetY;
					secondTouchX = touchOffsetX;
					secondTouchY = touchOffsetX;
					hgDialInfo.setFirstTouchX(firstTouchX);
					hgDialInfo.setFirstTouchY(firstTouchY);
                }
                else /* if(useSingleFinger == false) */ {
					firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
					secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
					hgDialInfo.setFirstTouchX(firstTouchX);
					hgDialInfo.setFirstTouchY(firstTouchY);
					hgDialInfo.setSecondTouchX(secondTouchX);
					hgDialInfo.setSecondTouchY(secondTouchY);
                }//End if(useSingleFinger == true)
            }
            catch(IndexOutOfBoundsException e) {return;}
        }
        catch(IllegalArgumentException e) {return;}

    }//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


    /* Top of block rotate functions */
    private HGDialInfo doDownDial() {

		spinTriggered = false;
		spinTriggeredProgrammatically = false;
		hgDialInfo.setSpinTriggered(false);
		fullTextureDownAngle = fullTextureAngle;
		gestureDownTime = System.currentTimeMillis();

        //Prepare touches for single or dual touch
		if(useSingleFinger == true) {
            touchXLocal = flingDownTouchX = firstTouchX;
            touchYLocal = flingDownTouchY = firstTouchY;
        }
        else if(touchPointerCount > 1 && useSingleFinger == false) {
            touchXLocal = secondTouchX;
            touchYLocal = secondTouchY;
        }
        else /* if(touchPointerCount > 2) */ {
            return hgDialInfo;
        }//End if(useSingleFinger == true)

		angleWrapper.setGestureTouchAngle(getAngleFromPoint(angleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));

        if(precisionRotation != 0) {

            if(cumulativeRotate == true) {

				onDownCumulativeTextureAngle = angleWrapper.getGestureTouchAngle() - onUpGestureAngle;

				if(useVariableDial == true) {
					fullTextureAngle = fullTextureAngle * precisionRotation;
				}

            }
            else if(cumulativeRotate == false) {

				minMaxRotationOutOfBounds = 0;
				onDownCumulativeTextureAngle = angleWrapper.getGestureTouchAngle() - onUpGestureAngle;

				if(useVariableDial == true) {
					fullTextureAngle = angleWrapper.getGestureTouchAngle();
				}
				else if(useVariableDial == false) {
					fullTextureAngle = angleWrapper.getGestureTouchAngle() / precisionRotation;
				}//End if(useVariableDial == true)

            }//End if(cumulativeRotate == true)

			if(useVariableDial == false) {hgDialInfo.setVariablePrecision(storedPrecisionRotation);}

			if(spinTriggered == true && angleSnapNextOld != 0) {if(useVariableDial == false) {doManualTextureDial(angleSnapNextOld);}}

			setReturnType();
			spinStartAngle = fullTextureAngle;

        }//End if(precisionRotation != 0)

        return hgDialInfo;

    }//End private HGDialInfo doDownDial()


    private HGDialInfo doMoveDial() {

        //Prepare touches for single or dual touch
		if(useSingleFinger == true) {
            touchXLocal = firstTouchX;
            touchYLocal = firstTouchY;
        }
        else if(touchPointerCount > 1 && useSingleFinger == false) {
            touchXLocal = secondTouchX;
            touchYLocal = secondTouchY;
        }
        else {
			if(spinTriggeredProgrammatically == false) {return hgDialInfo;}
        }//End if(useSingleFinger == true)

		angleWrapper.setGestureTouchAngle(getAngleFromPoint(angleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));
        setReturnType();

        return hgDialInfo;

    }//End private HGDialInfo doMoveDial()


    private HGDialInfo doUpDial() {

        //Prepare touches for single or dual touch
		if(useSingleFinger == true) {
            touchXLocal = firstTouchX;
            touchYLocal = firstTouchY;
        }
        else if(touchPointerCount > 1 && useSingleFinger == false) {
            touchXLocal = secondTouchX;
            touchYLocal = secondTouchY;
        }//End if(useSingleFinger == true)

		angleWrapper.setGestureTouchAngle(getAngleFromPoint(angleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));

        if(precisionRotation != 0) {
            onUpGestureAngle = (1 - (onDownCumulativeTextureAngle - angleWrapper.getGestureTouchAngle())) % 1;
			if(useVariableDial == true) {precisionRotation = 1f;}
            setReturnType();
        }//End if(precisionRotation != 0)

		setSpinStatus();

		if(spinTriggered == true) {

            spinAnimationThread = new Thread(this);

			spinAnimationThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread t, Throwable e) {
					//Fault Tolerance
				}
			});

			try {
				spinAnimationThread.start();
			}
			catch(IllegalThreadStateException e) {
				spinTriggered = false;
				spinAnimationThread = null;
			}

		}//End if(spinTriggered == true)

        return hgDialInfo;

    }//End private HGDialInfo doUpDial()
	/* Bottom of block rotate functions */


    private void setReturnType() {

        if(useVariableDial == false) {

            if(useMinMaxRotation == false) {
                hgDialInfo.setGestureRotationDirection(setRotationAngleGestureAndReturnDirection());
            }
            else if(useMinMaxRotation == true) {
                hgDialInfo.setGestureRotationDirection(setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour());
            }//End if(useMinMaxRotation == false)

			hgDialInfo.setGestureAngle(angleWrapper.getGestureAngle());
			hgDialInfo.setTextureRotationDirection(lastTextureDirection);
			hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
			hgDialInfo.setRotateSnapped(rotateSnapped);
			hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			hgDialInfo.setVariablePrecision(storedPrecisionRotation);
			calculateTextureAngle();

            if(angleSnap != 0) {
				checkAngleSnap();
            }

            if(suppressInvalidate == false) {
                invalidate();
            }

        }
        else if(useVariableDial == true) {

			if(useMinMaxRotation == false) {
				angleWrapper.setTextureRotationDirection(setVariableDialAngleAndReturnDirection());
			}
			else if(useMinMaxRotation == true) {
				angleWrapper.setTextureRotationDirection(setVariableDialAngleAndReturnDirectionForMinMaxBehaviour());
			}//End if(useMinMaxRotation == false)

            hgDialInfo.setGestureAngle(angleWrapper.getGestureTouchAngle());
            hgDialInfo.setTextureRotationDirection(lastTextureDirection);
            hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
            hgDialInfo.setRotateSnapped(rotateSnapped);
            hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);

            if(angleSnap != 0) {
				checkAngleSnap();
            }

            if(suppressInvalidate == false) {
                invalidate();
            }

        }//End if(useVariableDial == false)

        hgDialInfo.setGestureTouchAngle(angleWrapper.getGestureTouchAngle());

    }//End private void setReturnType()
    /* Bottom of block rotate functions */


	private void setSpinStatus() {

		final long currentTime = System.currentTimeMillis();
		final double twoFingerDistance = useSingleFinger ? getTwoFingerDistance(flingDownTouchX, flingDownTouchY, firstTouchX, firstTouchY) : 0d;
		final boolean flingTimeMet = currentTime < gestureDownTime + flingTimeThreshold;
		final boolean quickTapTimeMet = currentTime < gestureDownTime + quickTapTime;
		final boolean distanceMet = flingDistanceThreshold > 0 && twoFingerDistance > flingDistanceThreshold;
		final boolean angleToleranceMet = flingAngleTolerance != 0 && Math.abs(fullTextureDownAngle - fullTextureAngle) > flingAngleTolerance;

		if(quickTapTimeMet && !angleToleranceMet && !distanceMet) {
			hgDialInfo.setQuickTap(true);
			spinTriggered = false;
			return;
		}
		else {
			hgDialInfo.setQuickTap(false);
		}//End if(quickTapTimeMet && !angleToleranceMet && !distanceMet)

		if((angleToleranceMet || (flingAngleTolerance == 0 && distanceMet)) && flingTimeMet) {
			spinTriggered = true;
			hgDialInfo.setSpinTriggered(true);
		}
		else {
			spinTriggered = false;
			hgDialInfo.setSpinTriggered(false);
		}//End if((angleToleranceMet || (flingAngleTolerance == 0 && distanceMet)) && flingTimeMet)

		if(spinTriggered == true) {

			if(spinStartSpeed == 0 && spinEndSpeed == 0) {
				spinStartSpeed = (float) (((1000d / (currentTime - gestureDownTime))) * Math.abs((spinStartAngle - fullTextureAngle)));
			}
			else {
				spinStartSpeed = spinStartSpeedState;
			}

			if(slowFactor == 0) {
				spinEndTime = currentTime + spinDuration;
			}
			else if(slowFactor != 0) {
				spinDuration = (long) (spinStartSpeed * slowFactor * 1000L);
				spinEndTime = currentTime + spinDuration;
			}//End if(slowFactor == 0)

		}//End if(spinTriggered == true)

	}//End private void setSpinStatus()


    /* Top of block main functions */
    private int setRotationAngleGestureAndReturnDirection() {

        final int[] returnValue = new int[1];
        currentTextureAngle = (1 - (onDownCumulativeTextureAngle - angleWrapper.getGestureTouchAngle()));
        final float angleDifference = (float) (storedTextureAngle - currentTextureAngle);

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75f)) {

            if(angleDifference > 0) {
                returnValue[0] = -1;
                fullTextureAngle -= (angleDifference + 1f) % 1f;
            }
            else if(angleDifference < 0) {
                returnValue[0] = 1;
                fullTextureAngle += -angleDifference % 1f;
            }//End if(angleDifference > 0)

            if(precisionRotation < 0) {
				angleWrapper.setTextureRotationDirection(-returnValue[0]);
            }
            else if(precisionRotation > 0) {
				angleWrapper.setTextureRotationDirection(returnValue[0]);
            }
            else {
				angleWrapper.setTextureRotationDirection(0);
            }//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

            	if(spinTriggeredProgrammatically == false) {

					if(precisionRotation < 0) {
						lastTextureDirection = -returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else if(precisionRotation > 0) {
						lastTextureDirection = returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else {
						angleWrapper.setTextureRotationDirection(0);
					}//End if(precisionRotation < 0)

				}//End if(spinTriggeredProgrammatically == false)

			}//End if(returnValue[0] != 0)

        }//End if(!(Math.abs(angleDifference) > 0.75f))

		angleWrapper.setGestureAngle(fullTextureAngle);
        storedTextureAngle = currentTextureAngle;

        return returnValue[0];

    }//End private int setRotationAngleGestureAndReturnDirection()


	private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentTextureAngle = (1 - (onDownCumulativeTextureAngle - angleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedTextureAngle - currentTextureAngle);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {
				returnValue[0] = -1;
				fullTextureAngle -= (angleDifference + 1f) % 1f;
			}
			else if(angleDifference < 0) {
				returnValue[0] = 1;
				fullTextureAngle += -angleDifference % 1f;
			}//End if(angleDifference > 0)

			if(precisionRotation < 0) {
				angleWrapper.setTextureRotationDirection(-returnValue[0]);
			}
			else if(precisionRotation > 0) {
				angleWrapper.setTextureRotationDirection(returnValue[0]);
			}
			else {
				angleWrapper.setTextureRotationDirection(0);
			}//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

				if(spinTriggeredProgrammatically == false) {

					if(precisionRotation < 0) {
						lastTextureDirection = -returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else if(precisionRotation > 0) {
						lastTextureDirection = returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else {
						angleWrapper.setTextureRotationDirection(0);
					}//End if(precisionRotation < 0)

				}//End if(spinTriggeredProgrammatically == false)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		if(precisionRotation > 0) {

			if(fullTextureAngle < minimumRotation / precisionRotation) {
				if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false); spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullTextureAngle = minimumRotation / precisionRotation;
			}
			else if(fullTextureAngle > maximumRotation / precisionRotation) {
				if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false); spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullTextureAngle = maximumRotation / precisionRotation;
			}//End if(fullTextureAngle < minimumRotation / precisionRotation)

		}
		else if(precisionRotation < 0) {

			if(-fullTextureAngle < -(minimumRotation / precisionRotation)) {
				if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false); spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullTextureAngle = (minimumRotation / precisionRotation);
			}
			else if(-fullTextureAngle > -(maximumRotation / precisionRotation)) {
				if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false); spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullTextureAngle = (maximumRotation / precisionRotation);
			}//End if(-fullTextureAngle < -(minimumRotation / precisionRotation))

		}//End if(precisionRotation > 0)

		angleWrapper.setGestureAngle(fullTextureAngle);
		storedTextureAngle = currentTextureAngle;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour()


    private int setVariableDialAngleAndReturnDirection() {

        final int[] returnValue = new int[1];
        currentTextureAngle = (1 - (onDownCumulativeTextureAngle - angleWrapper.getGestureTouchAngle()));
        final double angleDifference = (storedTextureAngle - currentTextureAngle) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {
			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();
		}
		else /* if(useVariableDialCurve == true) */ {
			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();
		}//End if(useVariableDialCurve == false)

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75f)) {

            if(angleDifference > 0) {

                returnValue[0] = -1;

                if(minMaxRotationOutOfBounds == 0) {
                    fullTextureAngle -= ((angleDifference * variablePrecision));
                }

            }
            else if(angleDifference < 0) {

                returnValue[0] = 1;

                if(minMaxRotationOutOfBounds == 0) {
                    fullTextureAngle += -(angleDifference * variablePrecision);
                }

            }//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(spinTriggeredProgrammatically == false) {

					if(variablePrecision > 0) {
						lastTextureDirection = returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else if(variablePrecision < 0) {
						lastTextureDirection = -returnValue[0];
						angleWrapper.setTextureRotationDirection(-lastTextureDirection);
					}//End if(variablePrecision > 0)

				}//End if(spinTriggeredProgrammatically == false)

				hgDialInfo.setTextureRotationDirection(lastTextureDirection);

			}//End if(returnValue[0] != 0)

        }//End if(!(Math.abs(angleDifference) > 0.75f))

		angleWrapper.setTextureAngle(fullTextureAngle);
        storedTextureAngle = currentTextureAngle;

        return returnValue[0];

    }//End private int setVariableDialAngleAndReturnDirection()


    private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour() {

        final int[] returnValue = new int[1];
        currentTextureAngle = (1 - (onDownCumulativeTextureAngle - angleWrapper.getGestureTouchAngle()));
        final double angleDifference = (storedTextureAngle - currentTextureAngle) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {
			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();
		}
		else /* if(useVariableDialCurve == true) */ {
			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();
		}//End if(useVariableDialCurve == false)

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75f)) {

            if(angleDifference > 0) {

                returnValue[0] = -1;

                if(minMaxRotationOutOfBounds == 0) {
                    fullTextureAngle -= ((angleDifference * variablePrecision));
                }

            }
            else if(angleDifference < 0) {

                returnValue[0] = 1;

                if(minMaxRotationOutOfBounds == 0) {
                    fullTextureAngle += -(angleDifference * variablePrecision);
                }

            }//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(spinTriggeredProgrammatically == false) {

					if(variablePrecision > 0) {
						lastTextureDirection = returnValue[0];
						angleWrapper.setTextureRotationDirection(lastTextureDirection);
					}
					else if(variablePrecision < 0) {
						lastTextureDirection = -returnValue[0];
						angleWrapper.setTextureRotationDirection(-lastTextureDirection);
					}//End if(variablePrecision > 0)

				}//End if(spinTriggeredProgrammatically == false)

				hgDialInfo.setTextureRotationDirection(lastTextureDirection);

			}//End if(returnValue[0] != 0)

        }//End if(!(Math.abs(angleDifference) > 0.75f))

		if(fullTextureAngle < minimumRotation) {
			if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = -1;
			hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullTextureAngle = minimumRotation;
		}
		else if(fullTextureAngle > maximumRotation) {
			if(spinTriggered == true) {hgDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = 1;
			hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullTextureAngle = maximumRotation;
		}//End if(fullTextureAngle < minimumRotation)

        if(minMaxRotationOutOfBounds != 0) {

			if(variablePrecision > 0) {

				if(fullTextureAngle == minimumRotation) {
					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}
				}
				else if(fullTextureAngle == maximumRotation) {
					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}
				}//End if(fullTextureAngle == minimumRotation)

			}
			else if(variablePrecision < 0) {

				if(fullTextureAngle == minimumRotation) {
					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}
				}
				else if(fullTextureAngle == maximumRotation) {
					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}
				}//End if(fullTextureAngle == minimumRotation)

			}//End if(variablePrecision > 0)

        }//End if(minMaxRotationOutOfBounds != 0)

		angleWrapper.setTextureAngle(fullTextureAngle);
        storedTextureAngle = currentTextureAngle;

        return returnValue[0];

    }//End private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour()


    private double getVariablePrecision() {

        double distanceFromCenter = getTwoFingerDistance(angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y, touchXLocal, touchYLocal);
		double[] variablePrecision = new double[1];

        if(distanceFromCenter <= imageRadius) {
			variablePrecision[0] = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;
            hgDialInfo.setVariablePrecision(variablePrecision[0]);
        }

        return variablePrecision[0];

    }//End private double getVariablePrecision()


	private double getVariablePrecisionCurve() {

		final double distanceFromCenter = getTwoFingerDistance(angleWrapper.viewCenterPoint.x, angleWrapper.viewCenterPoint.y, touchXLocal, touchYLocal);
		double[] variablePrecision = new double[1];

		if(distanceFromCenter <= imageRadius) {

            final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);

			if(positiveCurve == false) {
				variablePrecision[0] = -rangeDiff * (1f - (Math.sin((distanceFromCenter / imageRadiusX2) * Math.PI))) + variableDialOuter;
				hgDialInfo.setVariablePrecision(variablePrecision[0]);
			}
			else if(positiveCurve == true) {
				variablePrecision[0] = -rangeDiff * (Math.cos((distanceFromCenter / imageRadiusX2) * Math.PI)) + variableDialOuter;
				hgDialInfo.setVariablePrecision(variablePrecision[0]);
			}//End if(positiveCurve == false)

		}//End if(distanceFromCenter <= imageRadius)

		return variablePrecision[0];

	}//End private double getVariablePrecisionCurve()

	private void calculateTextureAngle() {angleWrapper.setTextureAngle(getGestureFullAngle() * precisionRotation);}

	void checkAngleSnap() {

		final double tempAngleSnap = Math.round(angleWrapper.getTextureAngle() / angleSnap);

		if(angleWrapper.getTextureAngle() > (tempAngleSnap * angleSnap) - angleSnapProximity && angleWrapper.getTextureAngle() < (tempAngleSnap * angleSnap) + angleSnapProximity) {
			angleSnapNext = tempAngleSnap * angleSnap;
			rotateSnapped = true;
		}
		else {
			angleSnapNext = angleWrapper.getTextureAngle();
			rotateSnapped = false;
		}

		hgDialInfo.setRotateSnapped(rotateSnapped);

	}//End void checkAngleSnap()

	public void cancelSpin() {spinTriggered = false;}
    /* Bottom of block main functions */

    /* Top of block Accessors */
    public AngleWrapper angleWrapper() {return this.angleWrapper;}
    public long getQuickTapTime() {return this.quickTapTime;}
    public boolean getCumulativeRotate() {return this.cumulativeRotate;}
    public boolean getIsSingleFinger() {return this.useSingleFinger;}
    public double getPrecisionRotation() {return this.precisionRotation;}
    public double getAngleSnap() {return this.angleSnap;}
    public double getAngleSnapProximity() {return this.angleSnapProximity;}
    public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
    public float getTouchOffsetX() {return this.touchOffsetX;}
    public float getTouchOffsetY() {return this.touchOffsetY;}
    public boolean hasAngleSnapped() {return rotateSnapped;}
    public boolean getSuppressInvalidate() {return this.suppressInvalidate;}
    public double getVariableDialInner() {return this.variableDialInner;}
    public double getVariableDialOuter() {return this.variableDialOuter;}
    public boolean getUseVariableDial() {return this.useVariableDial;}
	public Drawable getDrawable() {return drawable;}
	public boolean getUseVariableDialCurve() {return this.useVariableDialCurve;}
    public boolean getPositiveCurve() {return this.positiveCurve;}
	public int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
    public long getFlingTimeThreshold() {return this.flingTimeThreshold;}
    public float getSpinStartSpeed() {return this.spinStartSpeed;}
	public float getSpinEndSpeed() {return this.spinEndSpeed;}
	public long getSpinDuration() {return this.spinDuration;}
	public boolean getSpinTriggered() {return this.spinTriggered;}
	public float getSlowFactor() {return this.slowFactor;}
	public boolean getSlowDown() {return this.slowDown;}
	public float getFlingAngleTolerance() {return this.flingAngleTolerance;}
    /* Bottom of block Accessors */

    /* Top of block Mutators */
    public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
    public void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
    public void setIsSingleFinger(final boolean useSingleFinger) {this.useSingleFinger = useSingleFinger;}
    public void setSuppressInvalidate(final boolean suppressInvalidate) {this.suppressInvalidate = suppressInvalidate;}
	public void setDrawable(final Drawable drawable) {this.drawable = drawable;}
	public void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {this.useVariableDialCurve = useVariableDialCurve; this.positiveCurve = positiveCurve;}
	public void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {this.flingDistanceThreshold = flingDistanceThreshold; this.flingTimeThreshold = flingTimeThreshold;}
	public void setSlowFactor(final float slowFactor) {this.slowFactor = slowFactor;}
	public void setSlowDown(final boolean slowDown) {this.slowDown = slowDown;}
	public void setFlingAngleTolerance(final float flingAngleTolerance) {this.flingAngleTolerance = flingAngleTolerance;}

	public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {
		this.spinStartSpeedState = this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeedState = this.spinEndSpeed = spinEndSpeed;
		this.spinDurationState = this.spinDuration = spinDuration;
    }

    public void setPrecisionRotation(final double precisionRotation) {

		this.storedPrecisionRotation = precisionRotation;

        if(this.precisionRotation != 0 && precisionRotation != 0) {
            double currentObjectAngle = fullTextureAngle * this.precisionRotation;
            double newObjectAngle = fullTextureAngle * precisionRotation;
            fullTextureAngle += ((currentObjectAngle - newObjectAngle) / precisionRotation);
        }

        this.precisionRotation = precisionRotation;
		hgDialInfo.setVariablePrecision(precisionRotation);

    }//End public void setPrecisionRotation(final double precisionRotation)

    public void setAngleSnap(final double angleSnap, final double angleSnapProximity) {
        this.angleSnap = (angleSnap) % 1;
        this.angleSnapProximity = (angleSnapProximity) % 1;
        if(angleSnapProximity > angleSnap / 2f) {this.angleSnapProximity = angleSnap / 2;}
    }//End public void setAngleSnap(final double angleSnap, final double angleSnapProximity)

    public void setTouchOffset(final float touchOffsetX, final float touchOffsetY) {this.touchOffsetX = touchOffsetX; this.touchOffsetY = touchOffsetY;}

    public void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial) {
        this.variableDialInner = variableDialInner;
        this.variableDialOuter = variableDialOuter;
        this.useVariableDial = useVariableDial;
    }//End public void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial)


    public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {

        this.minimumRotation = minimumRotation;
        this.maximumRotation = maximumRotation;
        this.useMinMaxRotation = useMinMaxRotation;

        if(useMinMaxRotation == true) {

            if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation) {
                this.useMinMaxRotation = false;
            }
            else {
                this.useMinMaxRotation = true;
            }//End if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation)

        }//End if(useMinMaxRotation == true)

        if(this.useMinMaxRotation == false) {minMaxRotationOutOfBounds = 0; hgDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

    }//End public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation)

    public void setRapidDial(final double rapidDial) {this.rapidDial = rapidDial; invalidate();}
    /* Bottom of block Mutators */

    /* Top of block convenience methods */
    public double getGestureFullAngle() {return this.fullTextureAngle;}
    private void doManualTextireDialInternal(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}

    public void doManualGestureDial(final double manualDial) {

        fullTextureAngle = manualDial;
		angleWrapper.setGestureAngle(manualDial);
		angleWrapper.setTextureAngle(angleWrapper.getTextureAngle());
        hgDialInfo.setGestureAngle(angleWrapper.getGestureAngle());
        hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
        onDownCumulativeTextureAngle = 0;
        onUpGestureAngle = 0;
		calculateTextureAngle();

        if(suppressInvalidate == false) {
            invalidate();
        }

    }//End public void doManualGestureDial(final double manualDial)

    public void doManualTextureDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}

	public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection) {

		this.spinStartSpeedState = this.spinStartSpeed;
		this.spinEndSpeedState = this.spinEndSpeed;
		this.spinDurationState = this.spinDuration;
		this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeed = spinEndSpeed;
		this.spinDuration = spinDuration;
		this.lastTextureDirection = objectDirection;
		spinEndTime = System.currentTimeMillis() + spinDuration;
		spinTriggered = true;
		spinTriggeredProgrammatically = true;
		spinAnimationThread = new Thread(this);
		spinAnimationThread.start();

	}//End public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection)

    public void performQuickTap() {

        hgDialInfo.setQuickTap(true);

        post(new Runnable() {
            @Override
            public void run() {
                ihgDial.onUp(doUpDial());
                hgDialInfo.setQuickTap(false);
            }
        });

    }//End public void performQuickTap()
	/* Bottom of block convenience methods */


    /* Top of block geometry functions */
    public float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

        float returnVal = 0;

        //+0 - 0.5
        if(touchPoint.x > centerPoint.x) {
            returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);
        }
        //+0.5
        else if(touchPoint.x < centerPoint.x) {
            returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));
        }//End if(touchPoint.x > centerPoint.x)

        return returnVal;

    }//End public float getAngleFromPoint(final Point centerPoint, final Point touchPoint)

    public Point getPointFromAngle(final double angle) {return getPointFromAngle(angle, angleWrapper.viewCenterPoint.x);}

	public Point getPointFromAngle(final double angle, final double radius) {

		final Point coords = new Point();
		coords.x = (int) (radius * Math.sin((angle) * 2 * Math.PI));
		coords.y = (int) -(radius * Math.cos((angle) * 2 * Math.PI));

		return coords;

	}//End public Point getPointFromAngle(final double angle, final double radius)


    public double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY) {

        float pinchDistanceX = 0;
        float pinchDistanceY = 0;

        if(firstTouchX > secondTouchX) {
            pinchDistanceX = Math.abs(secondTouchX - firstTouchX);
        }
        else if(firstTouchX < secondTouchX) {
            pinchDistanceX = Math.abs(firstTouchX - secondTouchX);
        }//End if(firstTouchX > secondTouchX)

        if(firstTouchY > secondTouchY) {
            pinchDistanceY = Math.abs(secondTouchY - firstTouchY);
        }
        else if(firstTouchY < secondTouchY) {
            pinchDistanceY = Math.abs(firstTouchY - secondTouchY);
        }//End if(firstTouchY > secondTouchY)

        if(pinchDistanceX == 0 && pinchDistanceY == 0) {
            return  0;
        }
        else {
            pinchDistanceX = (pinchDistanceX * pinchDistanceX);
            pinchDistanceY = (pinchDistanceY * pinchDistanceY);
            return (float) Math.abs(Math.sqrt(pinchDistanceX + pinchDistanceY));
        }//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

    }//End public double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)
    /* Bottom of block geometry functions */

    public void resetHGDial() {

        setFields();
        invalidate();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        sendTouchEvent(event);

        return true;

    }

    private OnTouchListener getOnTouchListerField() {

        onTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hgTouchEvent(event);
                return true;
            }
        };

        return onTouchListener;

    }//End private OnTouchListener getOnTouchListerField()

    public void sendTouchEvent(final MotionEvent event) {
        hgTouchEvent(event);
    }

    private void hgTouchEvent(final MotionEvent event) {

		touchPointerCount = event.getPointerCount();
        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        if(getPrecisionRotation() != 0) {

            switch(action) {
				case MotionEvent.ACTION_MOVE: {
					if(touchesSwapped == true) {return;}
					setMoveTouch(event);
					ihgDial.onMove(doMoveDial());
					break;
				}
				case MotionEvent.ACTION_DOWN: {
					setDownTouch(event);
					ihgDial.onDown(doDownDial());
					break;
				}
				case MotionEvent.ACTION_POINTER_DOWN: {
					if((touchPointerCount > 2 && useSingleFinger == false) || (touchPointerCount > 1 && useSingleFinger == true)) {
						tooManyTouches = true;
					}
					else {
						setDownTouch(event);
						ihgDial.onDown(doDownDial());
					}
					break;
				}
				case MotionEvent.ACTION_POINTER_UP: {
					if(tooManyTouches == false) {
						setUpTouch(event);
						ihgDial.onUp(doUpDial());
					}
					else if(tooManyTouches == true) {
						if(useSingleFinger == false) {
							if(event.findPointerIndex(2) != event.getActionIndex()) {
								touchesSwapped = true;
							}
							else {
								touchesSwapped = false;
							}
						}
						else if(useSingleFinger == true) {
							if(event.findPointerIndex(1) != event.getActionIndex()) {
								touchesSwapped = true;
							}
							else {
								touchesSwapped = false;
							}
						}//End if(useSingleFinger == false)

					}//End if(tooManyTouches = false)
					break;
				}
				case MotionEvent.ACTION_UP: {
					tooManyTouches = false;
					touchesSwapped = false;
					setUpTouch(event);
					ihgDial.onUp(doUpDial());
					break;
				}
				default:
					break;
            }//End switch(action)

        }//End if(getPrecisionRotation() != 0)

    }//End private void hgTouchEvent(final MotionEvent event)

	@Override
	public void run() {

		if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration != 0) {

			if(slowFactor == 0f) {
				spinDuration = spinDurationState;
			}
			else if(slowFactor != 0f) {
				spinDuration = (long) (slowFactor * spinStartSpeed * 1000f);
			}

			hgDialInfo.setSpinCurrentSpeed(spinStartSpeed);
			post(new Runnable() {
				@Override
				public void run() {
					if(ihgDial != null) {ihgDial.onUp(hgDialInfo);}
				}
			});

			if(slowDown == true) {
				doSpinAnimation();
			}
			else if(slowDown == false) {
				doContinuousSpinAnimation();
			}//End if(slowDown == true)

		}
		else if((spinStartSpeed != 0 || spinEndSpeed != 0) && spinDuration != 0) {

			if(slowFactor == 0f) {
				spinDuration = spinDurationState;
			}
			else if(slowFactor != 0f) {
				spinDuration = (long) (slowFactor * spinStartSpeed * 1000f);
			}

			hgDialInfo.setSpinCurrentSpeed(spinStartSpeed);
			post(new Runnable() {
				@Override
				public void run() {
					if(ihgDial != null) {ihgDial.onUp(hgDialInfo);}
				}
			});

			if(slowDown == true) {
				doSpinAnimation();
			}
			else if(slowDown == false) {
				doContinuousSpinAnimation();
			}//End if(slowDown == true)

		}
		else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration == 0) {

			//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
			hgDialInfo.setSpinTriggered(true);

			post(new Runnable() {
				@Override
				public void run() {
					if(ihgDial != null) {ihgDial.onUp(hgDialInfo);}
				}
			});

		}

		hgDialHandler.sendEmptyMessage(0);

	}//End public void run()


    /* Top of block Fling Functions called on thread */
	private void doSpinAnimation() {

		if(precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}
		long currentTime = System.currentTimeMillis();
		final double speedDifference = spinStartSpeed - spinEndSpeed;
		double spinCurrentSpeed;

		while(spinTriggered == true) {

			spinCurrentSpeed = (spinStartSpeed + (speedDifference * ((double) (spinEndTime - currentTime) / spinDuration))) - speedDifference;

			if(currentTime < spinEndTime) {

				//Update view every 10 milliseconds
				if(currentTime + 10L < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();
					final double lastAngle = fullTextureAngle;
					fullTextureAngle += ((0.01f * spinCurrentSpeed) * lastTextureDirection);
					int direction = lastAngle > fullTextureAngle ? -1 : 1;
					if(precisionRotation < 0) {direction = -direction;}
					angleWrapper.setTextureAngle(fullTextureAngle * precisionRotation);
					if(angleSnap != 0) {checkAngleSnap();}
					hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
					hgDialInfo.setTextureRotationDirection(direction);
					hgDialInfo.setSpinCurrentSpeed((float) spinCurrentSpeed);
					final int minMax;

					if((minMax = checkMinMaxBounds(angleWrapper.getTextureAngle())) == 0) {

						post(new Runnable() {
							@Override
							public void run() {
								if(ihgDial != null) {
									ihgDial.onMove(hgDialInfo);
									invalidate();
								}
							}
						});

					}
					else if(checkMinMaxBounds(angleWrapper.getTextureAngle()) != 0) {

						hgDialInfo.setMinMaxRotationOutOfBounds(minMax);
						spinTriggered = false;

					}//End if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0)

				}//End if(currentTime + 10l < System.currentTimeMillis())

			}
			else {

				spinTriggered = false;
				hgDialInfo.setSpinTriggered(false);

			}//End if(spinCurrentSpeed > 0)

		}//End while(spinTriggered == true)

	}//End private void doSpinAnimation()


	private void doContinuousSpinAnimation() {

		if(precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}
		long currentTime = System.currentTimeMillis();
		hgDialInfo.setSpinCurrentSpeed(spinStartSpeed);

		while(spinTriggered == true) {

			//Update view every 10 milliseconds
			if(currentTime + 10L < System.currentTimeMillis()) {

				currentTime = System.currentTimeMillis();
				final double lastAngle = fullTextureAngle;
				fullTextureAngle += ((0.01f * spinStartSpeed) * lastTextureDirection);
				int direction = lastAngle > fullTextureAngle ? -1 : 1;
				if(precisionRotation < 0) {direction = -direction;}
				angleWrapper.setTextureAngle(fullTextureAngle * precisionRotation);
				if(angleSnap != 0) {checkAngleSnap();}
				hgDialInfo.setTextureAngle(angleWrapper.getTextureAngle());
				hgDialInfo.setTextureRotationDirection(direction);
				final int minMax;

				if((minMax = checkMinMaxBounds(angleWrapper.getTextureAngle())) == 0) {

					post(new Runnable() {
						@Override
						public void run() {
							if(ihgDial != null) {
								ihgDial.onMove(hgDialInfo);
								invalidate();
							}
						}
					});

				}
				else if(checkMinMaxBounds(angleWrapper.getTextureAngle()) != 0) {

					hgDialInfo.setMinMaxRotationOutOfBounds(minMax);
					spinTriggered = false;

				}//End if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0)

			}//End if(currentTime + 10l < System.currentTimeMillis())

		}//End while(spinTriggered == true)

	}//End private void doContinuousSpinAnimation()
	/* Bottom of block Fling Functions called on thread */


	//Allows spin behaviour to work intuitively with min max behaviour
	private int checkMinMaxBounds(final double textureAngle) {

		if(useMinMaxRotation == false) {

			return 0;

		}
		else if(useMinMaxRotation == true) {

			if(textureAngle > maximumRotation) {

				return 1;

			}
			else if(textureAngle < minimumRotation) {

				return -1;

			}//End if(textureAngle > dialMetrics.maximumRotation)

		}//End if(useMinMaxRotation == false)

		return 0;

	}//End private int checkMinMaxBounds(final double textureAngle)


	private static class HGDialHandler extends Handler {

		private final WeakReference<HGDial> hgDialWeakReference;

		public HGDialHandler(HGDial hgDial) {
			hgDialWeakReference = new WeakReference<>(hgDial);
		}

		@Override
		public void handleMessage(Message msg) {

			if(hgDialWeakReference != null) {

				final HGDial hgDial = hgDialWeakReference.get();
				hgDial.spinStartSpeed = hgDial.spinStartSpeedState;
				hgDial.spinEndSpeed = hgDial.spinEndSpeedState;
				hgDial.spinDuration = hgDial.spinDurationState;
				hgDial.spinTriggeredProgrammatically = false;
				hgDial.lastTextureDirection = 0;

				if(hgDial.spinTriggered == false) {

					hgDial.hgDialInfo.setSpinTriggered(false);
					hgDial.hgDialInfo.setGestureRotationDirection(0);
					hgDial.hgDialInfo.setTextureRotationDirection(0);
					hgDial.hgDialInfo.setSpinCurrentSpeed(0f);
					if(hgDial.ihgDial != null) {hgDial.ihgDial.onUp(hgDial.hgDialInfo);}

					if(hgDial.useVariableDial == true) {
						hgDial.angleWrapper.setTextureAngle(hgDial.fullTextureAngle);
					}

					hgDial.post(new Runnable() {
						@Override
						public void run() {
							hgDial.invalidate();
						}
					});

				}//End if(hgDial.flingTriggered == true)

				hgDial.spinAnimationThread = null;

			}//End if(hgDialWeakReference != null)

		}

	}//End private static class HGDialHandler extends Handler


	@Override
	public void requestLayout() {
		viewIsSetup = false;
		super.requestLayout();
	}


	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		viewIsSetup = false;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}


	@Override
	protected void onConfigurationChanged(final Configuration newConfig) {
		viewIsSetup = false;
		super.onConfigurationChanged(newConfig);
	}


	@Override
	protected void onAttachedToWindow() {
		viewIsSetup = false;
		super.onAttachedToWindow();

	}


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		spinTriggered = false;
		spinTriggeredProgrammatically = false;
		viewIsSetup = false;

	}

}