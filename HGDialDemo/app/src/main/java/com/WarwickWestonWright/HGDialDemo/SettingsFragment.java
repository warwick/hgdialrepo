/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.HGDialDemo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.gargy.floatingkeyboardview.FloatingKeyboardView;

public class SettingsFragment extends DialogFragment implements DialogInterface.OnDismissListener {

	private DialogInterface.OnClickListener onClickListener;

	public SettingsFragment() {}

	private CheckBox chkPrecisionEnable;
	private CheckBox chkAngleSnapEnable;
	private CheckBox chkAngleSnapProximityEnable;
	private CheckBox chkSingleFingerEnable;
	private CheckBox chkCumulativeEnable;
	private EditText txtPrecision;
	private EditText txtAngleSnap;
	private EditText txtAngleSnapProximity;
	private CheckBox chkMinMaxEnable;
	private EditText txtMinimumAngle;
	private EditText txtMaximumAngle;
	private CheckBox chkVariableDialEnable;
	private CheckBox chkUseVariableDialCurve;
	private CheckBox chkPositiveCurve;
	private EditText txtVariableDialInner;
	private EditText txtVariableDialOuter;

	private CheckBox chkEnableFling;
	private EditText txtFlingDistance;
	private EditText txtFlingTime;
	private EditText txtStartSpeed;
	private EditText txtEndSpeed;
	private EditText txtSpinAnimationTime;
	private EditText txtSlowFactor;
	private CheckBox chkSlowDown;
	private EditText txtFlingAngle;
	private EditText txtQuickTapTolerance;

	private Button btnCloseAndDial;
	private View rootView;
	private SharedPreferences sp;

	private FloatingKeyboardView floatingKeyboardView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(onClickListener == null) {
			onClickListener = (DialogInterface.OnClickListener) getTargetFragment();
		}
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}//End public void onCreate(Bundle savedInstanceState)


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final Dialog dialog = new Dialog(getActivity());
		final RelativeLayout root = new RelativeLayout(getActivity());
		root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(root);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

				if(event.getAction() == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

					if(floatingKeyboardView != null && floatingKeyboardView.isShown()) {
						floatingKeyboardView.hide();
						return true;
					}

					return true;
				}
				else {
					return true;
				}

			}
		});

		return dialog;

	}//End public Dialog onCreateDialog(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.settings_fragment, container, false);
		floatingKeyboardView = rootView.findViewById(R.id.keyboardview);
		floatingKeyboardView.setKeyboard(new Keyboard(getActivity().getBaseContext(), R.xml.keyboard_number));
		floatingKeyboardView.setPreviewEnabled(false);// NOTE Do not show the preview balloons
		floatingKeyboardView.setAllignBottomCenter(true);

		chkPrecisionEnable = rootView.findViewById(R.id.chkPrecisionEnable);
		chkAngleSnapEnable = rootView.findViewById(R.id.chkAngleSnapEnable);
		chkAngleSnapProximityEnable = rootView.findViewById(R.id.chkAngleSnapProximityEnable);
		chkCumulativeEnable = rootView.findViewById(R.id.chkCumulativeEnable);
		chkSingleFingerEnable = rootView.findViewById(R.id.chkSingleFingerEnable);
		txtPrecision = rootView.findViewById(R.id.txtPrecision);
		txtAngleSnap = rootView.findViewById(R.id.txtAngleSnap);
		txtAngleSnapProximity = rootView.findViewById(R.id.txtAngleSnapProximity);
		chkMinMaxEnable = rootView.findViewById(R.id.chkMinMaxEnable);
		txtMinimumAngle = rootView.findViewById(R.id.txtMinimumAngle);
		txtMaximumAngle = rootView.findViewById(R.id.txtMaximumAngle);
		btnCloseAndDial = rootView.findViewById(R.id.btnCloseAndDial);
		chkVariableDialEnable = rootView.findViewById(R.id.chkVariableDialEnable);
		chkUseVariableDialCurve = rootView.findViewById(R.id.chkUseVariableDialCurve);
		chkPositiveCurve = rootView.findViewById(R.id.chkPositiveCurve);
		txtVariableDialInner = rootView.findViewById(R.id.txtVariableDialInner);
		txtVariableDialOuter = rootView.findViewById(R.id.txtVariableDialOuter);
		chkEnableFling = rootView.findViewById(R.id.chkEnableFling);
		txtFlingDistance = rootView.findViewById(R.id.txtFlingDistance);
		txtFlingTime = rootView.findViewById(R.id.txtFlingTime);
		txtStartSpeed = rootView.findViewById(R.id.txtStartSpeed);
		txtEndSpeed = rootView.findViewById(R.id.txtEndSpeed);
		txtSpinAnimationTime = rootView.findViewById(R.id.txtSpinAnimationTime);
		txtSlowFactor = rootView.findViewById(R.id.txtSlowFactor);
		chkSlowDown = rootView.findViewById(R.id.chkSlowDown);
		txtFlingAngle = rootView.findViewById(R.id.txtFlingAngle);
		txtQuickTapTolerance = rootView.findViewById(R.id.txtQuickTapTolerance);

		floatingKeyboardView.registerEditText(txtPrecision);
		floatingKeyboardView.registerEditText(txtAngleSnap);
		floatingKeyboardView.registerEditText(txtAngleSnapProximity);
		floatingKeyboardView.registerEditText(txtMinimumAngle);
		floatingKeyboardView.registerEditText(txtMaximumAngle);
		floatingKeyboardView.registerEditText(txtVariableDialInner);
		floatingKeyboardView.registerEditText(txtVariableDialOuter);
		floatingKeyboardView.registerEditText(txtFlingDistance);
		floatingKeyboardView.registerEditText(txtFlingTime);
		floatingKeyboardView.registerEditText(txtStartSpeed);
		floatingKeyboardView.registerEditText(txtEndSpeed);
		floatingKeyboardView.registerEditText(txtSpinAnimationTime);
		floatingKeyboardView.registerEditText(txtSlowFactor);
		floatingKeyboardView.registerEditText(txtFlingAngle);
		floatingKeyboardView.registerEditText(txtQuickTapTolerance);

		chkPrecisionEnable.setChecked(sp.getBoolean("chkPrecisionEnable", false));
		chkAngleSnapEnable.setChecked(sp.getBoolean("chkAngleSnapEnable", false));
		chkAngleSnapProximityEnable.setChecked(sp.getBoolean("chkAngleSnapProximityEnable", false));
		chkCumulativeEnable.setChecked(sp.getBoolean("chkCumulativeEnable", true));
		chkSingleFingerEnable.setChecked(sp.getBoolean("chkSingleFingerEnable", true));
		txtPrecision.setText(sp.getString("txtPrecision", "1"));
		txtAngleSnap.setText(sp.getString("txtAngleSnap", "0"));
		chkMinMaxEnable.setChecked(sp.getBoolean("chkMinMaxEnable", false));
		txtMinimumAngle.setText(sp.getString("txtMinimumAngle", "-3.6"));
		txtMaximumAngle.setText(sp.getString("txtMaximumAngle", "4.4"));
		txtAngleSnapProximity.setText(sp.getString("txtAngleSnapProximity", "0.03125"));
		chkVariableDialEnable.setChecked(sp.getBoolean("chkVariableDialEnable", false));
		chkUseVariableDialCurve.setChecked(sp.getBoolean("chkUseVariableDialCurve", false));
		chkPositiveCurve.setChecked(sp.getBoolean("chkPositiveCurve", false));
		txtVariableDialInner.setText(sp.getString("txtVariableDialInner", "4.5"));
		txtVariableDialOuter.setText(sp.getString("txtVariableDialOuter", "0.8"));
		chkEnableFling.setChecked(sp.getBoolean("chkEnableFling", false));
		txtFlingDistance.setText(sp.getString("txtFlingDistance", "200"));
		txtFlingTime.setText(sp.getString("txtFlingTime", "250"));
		txtStartSpeed.setText(sp.getString("txtStartSpeed", "10"));
		txtEndSpeed.setText(sp.getString("txtEndSpeed", "0"));
		txtSpinAnimationTime.setText(sp.getString("txtSpinAnimationTime", "5000"));
		txtSlowFactor.setText(sp.getString("txtSlowFactor", "0"));
		chkSlowDown.setChecked(sp.getBoolean("slowDown", true));
		txtFlingAngle.setText(sp.getString("txtFlingAngle", "0"));
		txtQuickTapTolerance.setText(sp.getString("txtQuickTapTolerance", "100"));

		chkPrecisionEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkPrecisionEnable", chkPrecisionEnable.isChecked()).apply();
			}

		});

		chkAngleSnapEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkAngleSnapEnable", chkAngleSnapEnable.isChecked()).apply();
			}
		});

		chkAngleSnapProximityEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkAngleSnapProximityEnable", chkAngleSnapProximityEnable.isChecked()).apply();
			}
		});

		chkCumulativeEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("chkCumulativeEnable", chkCumulativeEnable.isChecked()).apply();

			}
		});

		chkSingleFingerEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkSingleFingerEnable", chkSingleFingerEnable.isChecked()).apply();
			}
		});

		chkMinMaxEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkMinMaxEnable", chkMinMaxEnable.isChecked()).apply();
			}
		});

		chkVariableDialEnable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkVariableDialEnable", chkVariableDialEnable.isChecked()).apply();
			}
		});

		chkUseVariableDialCurve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkUseVariableDialCurve", chkUseVariableDialCurve.isChecked()).apply();
			}
		});

		chkPositiveCurve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkPositiveCurve", chkPositiveCurve.isChecked()).apply();
			}
		});

		chkEnableFling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("chkEnableFling", chkEnableFling.isChecked()).apply();
			}
		});

		chkSlowDown.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("slowDown", chkSlowDown.isChecked()).apply();
			}
		});

		btnCloseAndDial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				exitFragment();
				onClickListener.onClick(getDialog(), 0);
			}

		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void exitFragment() {

		if(!txtPrecision.getText().toString().equals("")) {
			sp.edit().putString("txtPrecision", txtPrecision.getText().toString()).commit();
		}
		else {
			txtPrecision.setText("1");
		}

		if(!txtAngleSnap.getText().toString().equals("")) {
			sp.edit().putString("txtAngleSnap", txtAngleSnap.getText().toString()).commit();
		}
		else {
			txtAngleSnap.setText("0");
		}

		if(!txtAngleSnapProximity.getText().toString().equals("")) {
			sp.edit().putString("txtAngleSnapProximity", txtAngleSnapProximity.getText().toString()).commit();
		}
		else {
			txtAngleSnap.setText(getText(R.string.txt_Default_Angle_Snap_Proximity));
		}

		if(!txtMinimumAngle.getText().toString().equals("")) {
			sp.edit().putString("txtMinimumAngle", txtMinimumAngle.getText().toString()).commit();
		}
		else {
			txtAngleSnap.setText("-3.6");
		}

		if(!txtMaximumAngle.getText().toString().equals("")) {
			sp.edit().putString("txtMaximumAngle", txtMaximumAngle.getText().toString()).commit();
		}
		else {
			txtAngleSnap.setText("0.03125");
		}

		if(!txtVariableDialInner.getText().toString().equals("")) {
			sp.edit().putString("txtVariableDialInner", txtVariableDialInner.getText().toString()).commit();
		}
		else {
			txtVariableDialInner.setText("4.5");
		}

		if(!txtVariableDialOuter.getText().toString().equals("")) {
			sp.edit().putString("txtVariableDialOuter", txtVariableDialOuter.getText().toString()).commit();
		}
		else {
			txtVariableDialOuter.setText("0.8");
		}

		if(txtFlingDistance.getText().toString().isEmpty() == true) {
			txtFlingDistance.setText("0");
		}

		if(txtFlingTime.getText().toString().isEmpty() == true) {
			txtFlingTime.setText("0");
		}

		if(txtStartSpeed.getText().toString().isEmpty() == true) {
			txtStartSpeed.setText("0");
		}

		if(txtEndSpeed.getText().toString().isEmpty() == true) {
			txtEndSpeed.setText("0");
		}

		if(txtSpinAnimationTime.getText().toString().isEmpty() == true) {
			txtSpinAnimationTime.setText("0");
		}

		if(txtSlowFactor.getText().toString().isEmpty() == true) {
			txtSlowFactor.setText("0");
		}

		if(txtFlingAngle.getText().toString().isEmpty() == true) {
			txtFlingAngle.setText("0");
		}

		if(txtQuickTapTolerance.getText().toString().isEmpty() == true) {
			txtQuickTapTolerance.setText(sp.getString("txtQuickTapTolerance", "100"));
		}

		sp.edit().putString("txtFlingDistance", txtFlingDistance.getText().toString()).commit();
		sp.edit().putString("txtFlingTime", txtFlingTime.getText().toString()).commit();
		sp.edit().putString("txtStartSpeed", txtStartSpeed.getText().toString()).commit();
		sp.edit().putString("txtEndSpeed", txtEndSpeed.getText().toString()).commit();
		sp.edit().putString("txtSpinAnimationTime", txtSpinAnimationTime.getText().toString()).commit();
		sp.edit().putString("txtSlowFactor", txtSlowFactor.getText().toString()).commit();
		sp.edit().putString("txtFlingAngle", txtFlingAngle.getText().toString()).commit();
		sp.edit().putString("txtQuickTapTolerance", txtQuickTapTolerance.getText().toString()).commit();

	}//End private void exitFragment()


	@Override
	public void onDismiss(final DialogInterface dialog) {
		exitFragment();
		onClickListener.onClick(getDialog(), 0);
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		/*
		try {
			//onClickListener = (DialogInterface.OnClickListener) activity;
		}
		catch (ClassCastException e) {
			//throw new ClassCastException(activity.toString() + " must implement DialogInterface.OnClickListener");
		}
		*/
	}


	@Override
	public void onDetach() {
		super.onDetach();
		onClickListener = null;
	}

}