/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.HGDialDemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.WarwickWestonWright.HGDial.HGDial;
import com.WarwickWestonWright.HGDial.HGDialInfo;

public class MainActivity extends AppCompatActivity implements
	CogFragment.ICogFragment,
	CoolDialFragment.ICoolDialFragment,
	LibraryFragment.ILibraryFragment,
	HGDial.OnTouchListener {

	private LibraryFragment libraryFragment;
	private CogFragment cogFragment;
	private CoolDialFragment coolDialFragment;
	private SharedPreferences sp;
	private TextView lblMainMenuTitle;
	private HGDial menuIceDial;
	private Point point;
	private FrameLayout.LayoutParams fLayoutParams;
	private Drawable foregroundDrawable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		sp = getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		if(libraryFragment == null) {libraryFragment = new LibraryFragment();}
		if(cogFragment == null) {cogFragment = new CogFragment();}
		if(coolDialFragment == null) {coolDialFragment = new CoolDialFragment();}

		if(sp.getBoolean("spIsSetup", false) == false) {

			//Initialise Dial settings.
			sp.edit().putBoolean("chkPrecisionEnable", false).apply();
			sp.edit().putBoolean("chkAngleSnapEnable", false).apply();
			sp.edit().putBoolean("chkAngleSnapProximityEnable", false).apply();
			sp.edit().putBoolean("chkCumulativeEnable", true).apply();
			sp.edit().putBoolean("chkSingleFingerEnable", true).apply();
			sp.edit().putString("txtPrecision", "0.5").apply();
			sp.edit().putString("txtAngleSnap", "0.125").apply();
			sp.edit().putString("txtAngleSnapProximity", "0.03125").apply();
			sp.edit().putBoolean("chkMinMaxEnable", false).apply();
			sp.edit().putString("txtMinimumAngle", "-3.6").apply();
			sp.edit().putString("txtMaximumAngle", "4.4").apply();
			sp.edit().putBoolean("chkVariableDialEnable", false).apply();
			sp.edit().putBoolean("chkUseVariableDialCurve", false).apply();
			sp.edit().putBoolean("chkPositiveCurve", false).apply();
			sp.edit().putString("txtVariableDialInner", "4.5").apply();
			sp.edit().putString("txtVariableDialOuter", "0.8").apply();
			sp.edit().putBoolean("chkEnableFling", false).apply();
			sp.edit().putString("txtFlingDistance", "200").apply();
			sp.edit().putString("txtFlingTime", "250").apply();
			sp.edit().putString("txtStartSpeed", "10").apply();
			sp.edit().putString("txtEndSpeed", "0").apply();
			sp.edit().putString("txtSpinAnimationTime", "5000").apply();
			sp.edit().putString("txtSlowFactor", "0").apply();
			sp.edit().putBoolean("slowDown", true).apply();
			sp.edit().putString("txtFlingAngle", "0").apply();
			sp.edit().putString("txtQuickTapTolerance", "100").apply();
			sp.edit().putBoolean("spIsSetup", true).apply();

		}//End if(sp.getBoolean("spIsSetup", false) ==false)

		lblMainMenuTitle = findViewById(R.id.lblMainMenuTitle);
		lblMainMenuTitle.setTextColor(Color.parseColor("#FF0000"));
		point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		fLayoutParams = new FrameLayout.LayoutParams((point.x), (point.x), Gravity.CENTER);
		foregroundDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.menu_ice_dial, null);
		menuIceDial = findViewById(R.id.menuIceDial);
		menuIceDial.resetHGDial();
		menuIceDial.setDrawable(foregroundDrawable);
		menuIceDial.setLayoutParams(fLayoutParams);
		menuIceDial.setOnTouchListener(this);
		menuIceDial.setAngleSnap(0.25d, 0.03125d);
		menuIceDial.setPrecisionRotation(1d);
		menuIceDial.setMinMaxDial(1d, 3d, true);
		menuIceDial.doManualTextureDial(2d);

	}//End protected void onCreate(Bundle savedInstanceState)

	@Override
	public void LibraryFragmentCallback() {

		if(libraryFragment != null) {
			getSupportFragmentManager().beginTransaction().remove(libraryFragment).detach(libraryFragment).commit();
		}

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		resetDial();

	}//End public void LibraryFragmentCallback()


	@Override
	public void cogFragmentCallback() {

		if(cogFragment != null) {
			getSupportFragmentManager().beginTransaction().remove(cogFragment).detach(cogFragment).commit();
		}

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		resetDial();

	}//End public void cogFragmentCallback()


	@Override
	public void CoolDialFragmentCallback(String className) {

		if(coolDialFragment != null) {
			getSupportFragmentManager().beginTransaction().remove(coolDialFragment).detach(coolDialFragment).commit();
		}

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		resetDial();

	}//End public void CoolDialFragmentCallback(String className)


	private void resetDial() {

		menuIceDial.resetHGDial();
		menuIceDial.setAngleSnap(0.25d, 0.03125d);
		menuIceDial.setPrecisionRotation(1d);
		lblMainMenuTitle.setText("Dial and release\nto select menu");
		menuIceDial.setMinMaxDial(1d, 3d, true);
		menuIceDial.doManualTextureDial(2d);

	}//End private void resetDial()


	@Override
	public boolean onTouch(View v, MotionEvent event) {

		menuIceDial.registerCallback(new HGDial.IHGDial() {
			@Override
			public void onDown(HGDialInfo hgDialInfo) {}

			@Override
			public void onPointerDown(HGDialInfo hgDialInfo) {}

			@Override
			public void onMove(HGDialInfo hgDialInfo) {

				if(menuIceDial.hasAngleSnapped() == true) {

					final double currentAngleMod1 = hgDialInfo.getTextureAngle() % 1;

					if(currentAngleMod1 >= 0.03125d && currentAngleMod1 < 0.28125d) {
						lblMainMenuTitle.setText("Release to view\nLibrary Demo");
					}
					else if(currentAngleMod1 >= 0.28125d && currentAngleMod1 < 0.53125d) {
						lblMainMenuTitle.setText("Release to view\nCog Demo");
					}
					else if(currentAngleMod1 >= 0.53125d && currentAngleMod1 < 0.78125d) {
						lblMainMenuTitle.setText("Release to view\nOverlay Dial Effect");
					}
					else {
						lblMainMenuTitle.setText("Dial and release\nto select menu");
					}//End if(hgDialInfo.getTextureAngle() % 1 >= 0.03125d && hgDialInfo.getTextureAngle() < 0.28125d)

				}//End if(currentAngleMod1 >= 0.03125d && currentAngleMod1 < 0.28125d)

			}//End public void onMove(HGDial.HGDialInfo hgDialInfo)

			@Override
			public void onPointerUp(HGDialInfo hgDialInfo) {}

			@Override
			public void onUp(HGDialInfo hgDialInfo) {

				final double currentAngleMod1 = hgDialInfo.getTextureAngle() % 1;

				if(currentAngleMod1 >= 0.03125d && currentAngleMod1 < 0.28125d) {
					libraryFragment = new LibraryFragment();
					getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, libraryFragment, "LibraryFragment").commit();
				}
				else if(currentAngleMod1 >= 0.28125d && currentAngleMod1 < 0.53125d) {
					cogFragment = new CogFragment();
					getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, cogFragment, "CogFragment").commit();
				}
				else if(currentAngleMod1 >= 0.53125d && currentAngleMod1 < 0.78125d) {
					coolDialFragment = new CoolDialFragment();
					getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, coolDialFragment, "CoolDialFragment").commit();
				}
				else {
					lblMainMenuTitle.setText("Dial and release\nto select menu");
				}//End if(currentAngleMod1 >= 0.03125d && currentAngleMod1 < 0.28125d)

			}//End public void onUp(HGDial.HGDialInfo hgDialInfo)

		});

		menuIceDial.sendTouchEvent(event);

		return true;

	}//End public boolean onTouch(View v, MotionEvent event)


	public void onBackPressed() {

		if(libraryFragment != null && libraryFragment.isAdded()) {
			getSupportFragmentManager().beginTransaction().remove(libraryFragment).commit();
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			resetDial();
		}
		else if(cogFragment != null && cogFragment.isAdded()) {
			getSupportFragmentManager().beginTransaction().remove(cogFragment).commit();
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			resetDial();
		}
		else if(coolDialFragment != null && coolDialFragment.isAdded()) {
			getSupportFragmentManager().beginTransaction().remove(coolDialFragment).commit();
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			resetDial();
		}
		else {
			super.onBackPressed();
		}//End if(libraryFragment != null && libraryFragment.isAdded())

	}//End public void onBackPressed()

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(cogFragment != null && cogFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(cogFragment).detach(cogFragment).commit();
			cogFragment = null; cogFragment = new CogFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, cogFragment, "CogFragment").commit();
		}
		else if(coolDialFragment != null && coolDialFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(coolDialFragment).detach(coolDialFragment).commit();
			coolDialFragment = null; coolDialFragment = new CoolDialFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, coolDialFragment, "CoolDialFragment").commit();
		}
		else if(libraryFragment != null && libraryFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(libraryFragment).detach(libraryFragment).commit();
			libraryFragment = null; libraryFragment = new LibraryFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.MainActivityLayout, libraryFragment, "LibraryFragment").commit();
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}//End if(cogFragment != null && cogFragment.isAdded() == true)

	}//End public void onConfigurationChanged(Configuration newConfig)

}